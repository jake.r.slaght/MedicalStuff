﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace Appointments2.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string Role
        {
            get; set;
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("ApptsDBEntities", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<Appointments2.Patient> Patients
        {
            get; set;
        }

        public System.Data.Entity.DbSet<Appointments2.Doctor> Doctors
        {
            get; set;
        }

        public System.Data.Entity.DbSet<Appointments2.Prescription> Prescriptions
        {
            get; set;
        }

        public System.Data.Entity.DbSet<Appointments2.Appointment> Appointments
        {
            get; set;
        }

        public System.Data.Entity.DbSet<Appointments2.Medication> Medications
        {
            get; set;
        }
    }
}
