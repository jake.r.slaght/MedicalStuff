﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Appointments2.Startup))]
namespace Appointments2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
