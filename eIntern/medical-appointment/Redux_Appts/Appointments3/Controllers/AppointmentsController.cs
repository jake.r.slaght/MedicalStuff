﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Appointments.Entities;
using Appointments.Models;
using Appointments.Infrastructure;

namespace Appointments.Controllers
{
    public class AppointmentsController : Controller
    {
        private ApptEFDBContext db = new ApptEFDBContext();
        private Helpers h = new Helpers();
        // GET: Appointments
        public ActionResult Index()
        {
            return View(db.Appointments.ToList());
        }

        // GET: Appointments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            string fn= db.Patients.Where(n => n.ID == appointment.PatientID).First().FirstName;
            var ln= db.Patients.Where(n => n.ID == appointment.PatientID).First().LastName;
            var dfn = db.Doctors.Where(n => n.ID == appointment.DoctorID).First().FirstName;
            var dln = db.Doctors.Where(n => n.ID == appointment.DoctorID).First().LastName;
            string PatientName = fn + " " + ln;
            string DoctorName = dfn + " " + dln;
            if (appointment == null)
            {
                return HttpNotFound();
            }
            ViewBag.name = PatientName;
            ViewBag.dname = DoctorName;
            return View(appointment);
        }

        // GET: Appointments/Create
        public ActionResult Create()
        {
            ViewBag.patientsList = h.PatientsDropdownList();
            ViewBag.doctorsList = h.DoctorsDropdownList();
            return View();
        }

        // POST: Appointments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="Admin")]
        public ActionResult Create([Bind(Include = "ID,PatientID,StartDate,EndDate,Description")] Appointment appointment)
        {
            ViewBag.patientsList = h.PatientsDropdownList();

            if (ModelState.IsValid)
            {
                appointment.Diagnosis = "";
                appointment.DoctorID = 3; //This is the Not Assigned Doctor
                db.Appointments.Add(appointment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(appointment);
        }

        // GET: Appointments/Edit/5
        [Authorize(Roles = "Admin,Doctor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            string fn = db.Patients.Where(n => n.ID == appointment.PatientID).First().FirstName;
            var ln = db.Patients.Where(n => n.ID == appointment.PatientID).First().LastName;
            string PatientName = fn + " " + ln;

            ViewBag.patientsList = h.PatientsDropdownList();
            ViewBag.doctorsList = h.DoctorsDropdownList();
            ViewBag.name = PatientName;

            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        // POST: Appointments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Doctor")]
        public ActionResult Edit([Bind(Include = "ID,PatientID,Diagnosis,Prescriptions")] Appointment appointment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(appointment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(appointment);
        }

        // GET: Appointments/Delete/5
        [Authorize(Roles="Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        // POST: Appointments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Appointment appointment = db.Appointments.Find(id);
            db.Appointments.Remove(appointment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
