﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Appointments.Entities;
using Appointments.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Appointments.Infrastructure;
using Appointments3.Models;

namespace Appointments.Controllers
{
    public class DoctorsController : Controller
    {
        private ApptEFDBContext db = new ApptEFDBContext();

        // GET: Doctors
        public ActionResult Index()
        {
            return View(db.Doctors.ToList());
        }

        // GET: Doctors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doctor doctor = db.Doctors.Find(id);
            if (doctor == null)
            {
                return HttpNotFound();
            }
            return View(doctor);
        }


        // GET: Doctors/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Doctors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "ID,Username,FirstName,LastName")] Doctor doctor)
        {
            if (ModelState.IsValid)
            {
                if (db.Doctors.Where(p => p.FirstName == doctor.FirstName && p.LastName == doctor.LastName).Count() <= 0)
                {
                    ApplicationUser newUser = new ApplicationUser
                    {
                        Email = doctor.Username,
                        UserName = doctor.Username
                    };

                    ApplicationUserManager UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    UserManager.Create(newUser, "Password1!");
                    var currentUser = UserManager.FindByName(doctor.Username);
                    UserManager.AddToRole(newUser.Id, "Doctor");
                    db.Doctors.Add(doctor);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            return View(doctor);
        }

        // GET: Doctors/Edit/5
        [Authorize(Roles="Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doctor doctor = db.Doctors.Find(id);
            if (doctor == null)
            {
                return HttpNotFound();
            }
            return View(doctor);
        }

        // POST: Doctors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "ID,Username")] Doctor doctor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(doctor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(doctor);
        }

        // GET: Doctors/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doctor doctor = db.Doctors.Find(id);
            if (doctor == null)
            {
                return HttpNotFound();
            }
            return View(doctor);
        }

        // POST: Doctors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Doctor doctor = db.Doctors.Find(id);
            db.Doctors.Remove(doctor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize(Roles ="Admin")]
        public ActionResult Patients(int? id)
        {
            Helpers h = new Helpers();
            ViewBag.patientsList = h.PatientsDropdownList();
            Doctor doctor = db.Doctors.Find(id);
            return View(doctor);
        }
        [Authorize(Roles = "Admin")]
        public void AddPatient(PatientData pd)
        {
            Patient nPatient = db.Patients.Find(pd.pID);
            if (db.Doctors.Find(pd.dID).Patients.Count()==0)
            {
                db.Doctors.Find(pd.dID).Patients.Add(nPatient);
                db.SaveChanges();
            }
            else
            {
                foreach (Patient p in db.Doctors.Find(pd.dID).Patients)
                {
                    if (!(p.ID == nPatient.ID))
                    {
                        db.Doctors.Find(pd.dID).Patients.Add(nPatient);
                        db.SaveChanges();

                    }
                }
                DocPatientsView(pd.dID);
            }
        }
        [Authorize(Roles = "Admin")]
        public void RemovePatient(PatientData pd)
        {
            Patient nPatient = db.Patients.Find(pd.pID);
            db.Doctors.Find(pd.dID).Patients.Remove(nPatient);
            db.SaveChanges();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult DocPatientsView(int id)
        {
            var doc = db.Doctors.Find(id);
            return View(doc);
        }
    }
}
