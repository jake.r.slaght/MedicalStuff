﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Appointments.Entities;
using Appointments.Models;
using Appointments.Infrastructure;

namespace Appointments3.Controllers
{
    public class PrescriptionsController : Controller
    {
        private ApptEFDBContext db = new ApptEFDBContext();

        // GET: Prescriptions
        public ActionResult Index()
        {
            return View(db.Prescriptions.ToList());
        }

        // GET: Prescriptions/Details/5
        public ActionResult Details(int? id)
        {
           


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prescription prescription = db.Prescriptions.Find(id);
            string fn = db.Patients.Where(n => n.ID == prescription.PatientID).First().FirstName;
            var ln = db.Patients.Where(n => n.ID == prescription.PatientID).First().LastName;
            var dfn = db.Doctors.Where(n => n.ID == prescription.DoctorID).First().FirstName;
            var dln = db.Doctors.Where(n => n.ID == prescription.DoctorID).First().LastName;
            string med = db.Medications.Where(m => m.ID == prescription.MedicationID).First().Name;
            string PatientName = fn + " " + ln;
            string DoctorName = dfn + " " + dln;
            if (prescription == null)
            {
                return HttpNotFound();
            }
            ViewBag.name = PatientName;
            ViewBag.dname = DoctorName;
            ViewBag.med = med;
            return View(prescription);
        }

        // GET: Prescriptions/Create
        [Authorize (Roles ="Admin,Doctor")]
        public ActionResult Create()
        {
            Helpers h = new Helpers();
            ViewBag.patientsList = h.PatientsDropdownList();
            ViewBag.doctorsList = h.DoctorsDropdownList();
            ViewBag.drugsList = h.MedicationsDropdownList();

            return View();
        }

        // POST: Prescriptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Doctor")]
        public ActionResult Create([Bind(Include = "ID,DoctorID, PatientID, MedicationID, Directions,Quantity")] Prescription prescription)
        {
            if (ModelState.IsValid)
            {
                db.Prescriptions.Add(prescription);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(prescription);
        }

        // GET: Prescriptions/Delete/5
        [Authorize(Roles ="Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prescription prescription = db.Prescriptions.Find(id);
            if (prescription == null)
            {
                return HttpNotFound();
            }
            return View(prescription);
        }

        // POST: Prescriptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Prescription prescription = db.Prescriptions.Find(id);
            db.Prescriptions.Remove(prescription);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
