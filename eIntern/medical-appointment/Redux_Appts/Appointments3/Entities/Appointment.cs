﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Appointments.Entities
{
    public class Appointment
    {
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public int ID
        {
            get;set;
        }
        [Required]
        public int PatientID
        {
            get; set;
        }

        public int DoctorID
        {
            get; set;
        }
        [Required]
        public string Description
        {
            get; set;
        }
        public string Diagnosis
        {
            get;set;
        }
        [Required]
        public DateTime StartDate
        {
            get;set;
        }
        public DateTime EndDate
        {
            get;set;
        }
    }
}