﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Appointments.Entities
{
    public class Doctor
    {
        [Required]
        public string FirstName
        {
            get;
            set;
        }
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public int ID
        {
            get;set;
        }

        [Required]
        public string LastName
        {
            get;
            set;
        }
        public virtual ICollection<Patient> Patients
        {
            get;set;
        }
        public virtual ICollection<Appointment> Appointments
        {
            get; set;
        }
        [Required]
        public string Username
        {
            get;
            set;
        }
    }
}