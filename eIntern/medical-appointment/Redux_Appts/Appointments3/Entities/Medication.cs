﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Appointments.Entities
{
    public class Medication
    {
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public int ID
        {
            get; set;
        }
        [Required]
        public string Name
        {
            get; set;
        }
    }
}