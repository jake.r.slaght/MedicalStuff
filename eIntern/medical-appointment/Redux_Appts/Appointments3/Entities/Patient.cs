﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Appointments.Entities
{
    public class Patient
    {
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public int ID
        {
            get; set;
        }
        [Required]
        public string FirstName
        {
            get; set;
        }
        [Required]
        public string LastName
        {
            get; set;
        }

        public virtual ICollection<Appointment> Appointments
        {
            get;set;
        }
        public virtual ICollection<Doctor> Doctors
        {
            get; set;
        }
        public virtual ICollection<Prescription> Prescriptions
        {
            get;set;
        }
        [Required]
        public string Username
        {
            get;
            set;
        }
    }
}