﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Appointments.Entities
{
    public class Prescription
    {
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public int ID
        {
            get;set;
        }
        [Required]
        public int MedicationID
        {
            get;set;
        }
        [Required]
        public int DoctorID
        {
            get;set;
        }
        [Required]
        public int PatientID
        {
            get;set;
        }
        [Required]
        public string Directions
        {
            get;set;
        }
        [Required]
        public string Quantity
        {
            get;set;
        }
    }
}