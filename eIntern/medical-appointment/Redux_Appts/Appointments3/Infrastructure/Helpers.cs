﻿using Appointments.Entities;
using Appointments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Appointments.Infrastructure
{
    public class Helpers
    {
        private ApptEFDBContext db = new ApptEFDBContext();
        private ApplicationDbContext idb = new ApplicationDbContext();

        public List<object> PatientsDropdownList()
        {
            List<object> patientList = new List<object>();

            var roles = db.Patients.ToList();

            foreach (var role in roles)
            {
                patientList.Add(new
                {
                    id = role.ID,
                    name = role.FirstName + " "+ role.LastName
                });
            }

            return patientList;
        }
        public List<object> MedicationsDropdownList()
        {
            List<object> medList = new List<object>();

            var meds = db.Medications.ToList();

            foreach (var med in meds)
            {
                medList.Add(new
                {
                    id = med.ID,
                    name = med.Name
                });
            }

            return medList;
        }
        public List<object> DoctorsDropdownList()
        {
            List<object> doctorList = new List<object>();

            var roles = db.Doctors.ToList();

            foreach (var role in roles)
            {
                doctorList.Add(new
                {
                    id = role.ID,
                    name = role.FirstName + " " + role.LastName
                });
            }

            return doctorList;
        }



















    }
}
