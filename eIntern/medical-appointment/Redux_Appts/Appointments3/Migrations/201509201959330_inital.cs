namespace Appointments3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inital : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Diagnosis = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Doctor_ID = c.Int(),
                        Patient_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Doctors", t => t.Doctor_ID)
                .ForeignKey("dbo.Patients", t => t.Patient_ID, cascadeDelete: true)
                .Index(t => t.Doctor_ID)
                .Index(t => t.Patient_ID);
            
            CreateTable(
                "dbo.Doctors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Username = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Username = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Prescriptions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Directions = c.String(nullable: false),
                        Quantity = c.String(nullable: false),
                        Doctor_ID = c.Int(nullable: false),
                        Medication_ID = c.Int(nullable: false),
                        Patient_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Doctors", t => t.Doctor_ID, cascadeDelete: true)
                .ForeignKey("dbo.Medications", t => t.Medication_ID, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_ID, cascadeDelete: true)
                .Index(t => t.Doctor_ID)
                .Index(t => t.Medication_ID)
                .Index(t => t.Patient_ID);
            
            CreateTable(
                "dbo.Medications",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PatientDoctors",
                c => new
                    {
                        Patient_ID = c.Int(nullable: false),
                        Doctor_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Patient_ID, t.Doctor_ID })
                .ForeignKey("dbo.Patients", t => t.Patient_ID, cascadeDelete: true)
                .ForeignKey("dbo.Doctors", t => t.Doctor_ID, cascadeDelete: true)
                .Index(t => t.Patient_ID)
                .Index(t => t.Doctor_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Appointments", "Patient_ID", "dbo.Patients");
            DropForeignKey("dbo.Prescriptions", "Patient_ID", "dbo.Patients");
            DropForeignKey("dbo.Prescriptions", "Medication_ID", "dbo.Medications");
            DropForeignKey("dbo.Prescriptions", "Doctor_ID", "dbo.Doctors");
            DropForeignKey("dbo.PatientDoctors", "Doctor_ID", "dbo.Doctors");
            DropForeignKey("dbo.PatientDoctors", "Patient_ID", "dbo.Patients");
            DropForeignKey("dbo.Appointments", "Doctor_ID", "dbo.Doctors");
            DropIndex("dbo.PatientDoctors", new[] { "Doctor_ID" });
            DropIndex("dbo.PatientDoctors", new[] { "Patient_ID" });
            DropIndex("dbo.Prescriptions", new[] { "Patient_ID" });
            DropIndex("dbo.Prescriptions", new[] { "Medication_ID" });
            DropIndex("dbo.Prescriptions", new[] { "Doctor_ID" });
            DropIndex("dbo.Appointments", new[] { "Patient_ID" });
            DropIndex("dbo.Appointments", new[] { "Doctor_ID" });
            DropTable("dbo.PatientDoctors");
            DropTable("dbo.Medications");
            DropTable("dbo.Prescriptions");
            DropTable("dbo.Patients");
            DropTable("dbo.Doctors");
            DropTable("dbo.Appointments");
        }
    }
}
