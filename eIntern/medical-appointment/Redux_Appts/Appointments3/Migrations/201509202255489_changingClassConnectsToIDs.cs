namespace Appointments3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changingClassConnectsToIDs : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Prescriptions", "Doctor_ID", "dbo.Doctors");
            DropForeignKey("dbo.Prescriptions", "Medication_ID", "dbo.Medications");
            DropForeignKey("dbo.Appointments", "Doctor_ID", "dbo.Doctors");
            DropIndex("dbo.Appointments", new[] { "Doctor_ID" });
            DropIndex("dbo.Prescriptions", new[] { "Doctor_ID" });
            DropIndex("dbo.Prescriptions", new[] { "Medication_ID" });
            RenameColumn(table: "dbo.Appointments", name: "Doctor_ID", newName: "DoctorID");
            RenameColumn(table: "dbo.Appointments", name: "Patient_ID", newName: "PatientID");
            RenameColumn(table: "dbo.Prescriptions", name: "Patient_ID", newName: "PatientID");
            RenameIndex(table: "dbo.Appointments", name: "IX_Patient_ID", newName: "IX_PatientID");
            RenameIndex(table: "dbo.Prescriptions", name: "IX_Patient_ID", newName: "IX_PatientID");
            AddColumn("dbo.Prescriptions", "MedicationID", c => c.Int(nullable: false));
            AddColumn("dbo.Prescriptions", "DoctorID", c => c.Int(nullable: false));
            AlterColumn("dbo.Appointments", "DoctorID", c => c.Int(nullable: false));
            CreateIndex("dbo.Appointments", "DoctorID");
            AddForeignKey("dbo.Appointments", "DoctorID", "dbo.Doctors", "ID", cascadeDelete: true);
            DropColumn("dbo.Prescriptions", "Doctor_ID");
            DropColumn("dbo.Prescriptions", "Medication_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Prescriptions", "Medication_ID", c => c.Int(nullable: false));
            AddColumn("dbo.Prescriptions", "Doctor_ID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Appointments", "DoctorID", "dbo.Doctors");
            DropIndex("dbo.Appointments", new[] { "DoctorID" });
            AlterColumn("dbo.Appointments", "DoctorID", c => c.Int());
            DropColumn("dbo.Prescriptions", "DoctorID");
            DropColumn("dbo.Prescriptions", "MedicationID");
            RenameIndex(table: "dbo.Prescriptions", name: "IX_PatientID", newName: "IX_Patient_ID");
            RenameIndex(table: "dbo.Appointments", name: "IX_PatientID", newName: "IX_Patient_ID");
            RenameColumn(table: "dbo.Prescriptions", name: "PatientID", newName: "Patient_ID");
            RenameColumn(table: "dbo.Appointments", name: "PatientID", newName: "Patient_ID");
            RenameColumn(table: "dbo.Appointments", name: "DoctorID", newName: "Doctor_ID");
            CreateIndex("dbo.Prescriptions", "Medication_ID");
            CreateIndex("dbo.Prescriptions", "Doctor_ID");
            CreateIndex("dbo.Appointments", "Doctor_ID");
            AddForeignKey("dbo.Appointments", "Doctor_ID", "dbo.Doctors", "ID");
            AddForeignKey("dbo.Prescriptions", "Medication_ID", "dbo.Medications", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Prescriptions", "Doctor_ID", "dbo.Doctors", "ID", cascadeDelete: true);
        }
    }
}
