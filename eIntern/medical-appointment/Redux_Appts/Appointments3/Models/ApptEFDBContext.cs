﻿using Appointments.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Appointments.Models
{
    public class ApptEFDBContext :DbContext
    {
        public ApptEFDBContext(): base("AppointmentDB")
        {
        }
        public DbSet<Patient> Patients
        {
            get; set;
        }
        public DbSet<Doctor> Doctors
        {
            get; set;
        }
        public DbSet<Appointment> Appointments
        {
            get; set;
        }
        public DbSet<Prescription> Prescriptions
        {
            get; set;
        }
        public DbSet<Medication> Medications
        {
            get; set;
        }
    }


}